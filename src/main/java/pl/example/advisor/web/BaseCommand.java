package pl.example.advisor.web;

import java.io.Serializable;

/**
 * Created by Marek on 12.05.2016.
 */
public class BaseCommand implements Serializable{
    private String var1;

    public String getVar1() {
        return var1;
    }

    public void setVar1(String var1) {
        this.var1 = var1;
    }
}
