package pl.example.advisor.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Marek on 10.05.2016.
 */
@EnableWebMvc
@ControllerAdvice
public class ErrorHandlerController {
    private static final String ERROR_404_VIEW = "/error404";
    private static final String NUM_FORMAT_EXC_VIEW = "/numFormatExc";

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoHandlerFoundException.class)
    public String noHandlerError(){
        System.out.println("404 HTTP STATUS ERROR. IT IS BEING Handled!");
        return ERROR_404_VIEW;
    }
    
    @ExceptionHandler(NumberFormatException.class)
    public String numberFormatExceptionHandler(@ModelAttribute("command") BaseCommand command, HttpServletRequest request, Exception ex ){
        System.out.println("Error in number format passing. IT IS BEING Handled!");
        command.setVar1(request.getAttributeNames().toString());
        return NUM_FORMAT_EXC_VIEW;
    }
}
