package pl.example.advisor.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Marek on 10.05.2016.
 */
@Controller
@RequestMapping("/test")
public class MainController {

    private static final String MAIN_VIEW = "/main";
    private static final String PASS_PARAM_VIEW = "/passedParam";


    @RequestMapping
    public String showMainScreen(@ModelAttribute("command") BaseCommand command){
        System.out.println("Main Controller - Main Mapping");
        return MAIN_VIEW;
    }

    @RequestMapping(path = "/{id}")
    public String showTest(@ModelAttribute("command")
            BaseCommand command, @PathVariable Long id){
        command.setVar1(Long.toString(id));

        return PASS_PARAM_VIEW;
    }
}
